<?php
    session_start();
    require_once('functions.php');
    gsftp_session_bouncer(TRUE, 'gsftp.php');

    echo head('Login'); 
?>
<body>
    <div class="valign-wrapper row v-center">
        <div class="col card hoverable div-center">
            <div class="card-content">
                <span class="card-title">Verbindungsdaten</span>
                <form action="gsftp.php" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="login-server" name="login-server" type="text" class="validate" required="required" />
                            <label for="login-server">Server</label>
                        </div>
                        <div class="input-field col s6">
                            <input value="21" id="login-port" name="login-port" type="number" class="validate" required="required" />
                            <label for="login-port">Port</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="login-username" name="login-username" type="text" class="validate" required="required" />
                            <label for="login-username">Benutzername</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="login-password" name="login-password" type="password" class="validate" required="required" />
                            <label for="login-password">Passwort</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="login-root" name="login-root" type="text" class="validate" />
                            <label for="login-root">Verzeichnis</label>
                        </div>
                        <div class="col s6 input-field">
                            <input type="checkbox" name="login-ssl" id="login-ssl" checked="checked" /><label for="login-ssl">SSL</label>
                        </div>
                    </div>
                    <div class="card-action center-align">
                        <button class="btn waves-effect waves-light teal lighten-2" id="login-send" name="login-send">Verbinden
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<html>