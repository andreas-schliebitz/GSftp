<?php
    // Turn off error reporting for release, so we dont have to @ warnings.
    error_reporting(0);
    
    // Constants
    define('LOGIN_FIELDS', ['login-server', 'login-port', 'login-username', 'login-password']);
    define('CSRF_TOKEN_LENGTH', 256); // Bit
    define('AES_256_CBC', 'aes-256-cbc');
    define('AES_KEY_LENGTH', 256); // Bit
    define('AES_KEY_INVALIDATE', 3600 * 24); // 24h Hours
    define('PHP_FILE_UPLOAD_ERRORS', [
        1 => 'Die hochgeladene Datei überschreitet die in der Anweisung "upload_max_filesize" in php.ini festgelegte Größe',
        2 => 'Die hochgeladene Datei überschreitet die in dem HTML Formular mittels der Anweisung "MAX_FILE_SIZE" angegebene maximale Dateigröße.',
        3 => 'Die Datei wurde nur teilweise hochgeladen.',
        4 => 'Es wurde keine Datei hochgeladen.',
        6 => 'Fehlender temporärer Ordner.',
        7 => 'Speichern der Datei auf die Festplatte ist fehlgeschlagen.',
        8 => 'Eine PHP Erweiterung hat den Upload der Datei gestoppt.',
    ]);

    // All action listeners
    function gsftp_listen() {
        gsftp_logout_listener();
        gsftp_initial_chdir_listener();
        gsftp_chdir_listener();
        gsftp_download_listener();
        gsftp_upload_listener();
        gsftp_deletion_listener();
        gsftp_new_folder_listener();
        gsftp_rename_listener();
        gsftp_new_file_listener();
    }

    function head($title) {
    echo '<!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de" dir="ltr">
    <head>
        <title>'.$title.' | GSftp</title>

        <!-- Set UTF-8 encoding -->
        <meta charset="utf-8" />

        <!-- x-ua-compatible -->
        <meta http-equiv="x-ua-compatible" content="ie=edge" />

        <!-- Let browser know website is optimized for mobile -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

        <!-- Chrome Theme Color -->
        <meta name="theme-color" content="#b2dfdb" />

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/icons/192x192.png" />

        <!-- Manifest -->
        <link rel="manifest" href="manifest.json" />

        <!--Import Google Icon Font-->
        <link type="text/css" rel="stylesheet" href="css/materialicons.css" />

        <!-- Import materialize.css -->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen" />

        <!-- Import custom CSS -->
        <link type="text/css" rel="stylesheet" href="css/custom.css" />

        <!-- Import jQuery before materialize.js -->
        <script src="js/jquery.min.js"></script>
        <script src="js/materialize.min.js"></script>

        <!-- Import Annyang speech recognition -->
        <script src="js/annyang.min.js"></script>

        <!-- Import custom javascript -->
        <script src="js/functions.js"></script>
        <script src="js/click.js"></script>
        <script src="js/voice.js"></script>
        <script src="js/form.js"></script>
    </head>
    ';
    }

    function error_message($error_type, $error_message, $center = FALSE) {
        global $ftp_con;

        // Error message vertically centered if not logged in, otherwise on top
        $class = $center ? ' valign-wrapper v-center' : '';
        $pos = $center ? 'pull' : 'push';

        $href = session_active() ? '#' : './';

        echo '
            <div class="gsftp-error row ' . $class . '">
                <div class="card col s12 m12 l8 ' . $pos .'-l2 xl6 ' . $pos . '-xl3 red darken-4 bottom-margin">
                    <div class="card-content white-text">
                        <span class="card-title">Fehler: ' . $error_type . '</span>
                        <p>' . $error_message . '</p>
                        <p class="right-align">
                            <a class="gsftp-error-close white-text btn-flat" href="' . $href . '">Schließen</a>
                        </p>
                    </div>
                </div>
            </div>
        ';
    }

    function check_input($input_ids) {
        // For every input "name", check if set and not empty
        for ($i = 0, $len = count($input_ids); $i < $len; $i++) {
            $cur_id = $input_ids[$i];
            if (!isset($_POST[$cur_id]) || $_POST[$cur_id] == '') {
                return FALSE;
            }
        }
        return TRUE;
    }

    function gsftp_connect($server, $port, $username, $password, $ssl) {
        $ftp_con = FALSE;

        // Switch connection method, if SSL is requested
        if ($ssl) {
            $ftp_con = ftp_ssl_connect($server, $port);
        } else {
            $ftp_con = ftp_connect($server, $port);
        }
        
        // If FTP connection fails
        if (!$ftp_con) {
            if ($ssl) {
                echo error_message('SSL-Verbindung', 'Sichere SSL-Verbindung zum Server <b>"' . $server . ':' . $port . '"</b> konnte nicht aufgebaut werden.', TRUE);
            } else {
                echo error_message('Verbindung', 'Verbindung zum Server <b>"' . $server . ':' . $port . '"</b> konnte nicht aufgebaut werden.', TRUE);
            }
        } else {
            // Login to Server with username and password.
            $ftp_login = ftp_login($ftp_con, $username, $password);

            // If FTP login fails
            if (!$ftp_login) {
                echo error_message('Anmeldung', 'Anmeldung als <b>"' . $username . '@' . $server . ':' . $port . '"</b> war nicht möglich.', TRUE);
            } else {
                // Enable FTP passive mode, client is responsible for FTP communication
                ftp_pasv($ftp_con, TRUE);
                
                // Set session variables if session has not been activated yet
                if (!session_active()) {
                    // Generate random authentication token (CSRF)
                    $auth_token = bin2hex(random_bytes(CSRF_TOKEN_LENGTH / 8));

                    // Mark session as active
                    $_SESSION['auth'] = $auth_token;
                    $_SESSION['active'] = $auth_token;

                    // Create random encryption key
                    $encryption_key = random_bytes(AES_KEY_LENGTH / 8);

                    // Save encryption key client side in Cookie transfered via HTTPS which invalidates in 24h
                    setcookie('gsftp_aes_key', $encryption_key, time() + AES_KEY_INVALIDATE, NULL, NULL, TRUE, TRUE);
                    $_COOKIE['gsftp_aes_key'] = $encryption_key;

                    // Encrypt login credentials in SESSION
                    for ($i = 0, $len = count(LOGIN_FIELDS); $i < $len; $i++) {
                        $func_arg = func_get_args()[$i];
                        $_SESSION[LOGIN_FIELDS[$i]] = gsftp_encrypt($func_arg, $encryption_key);
                    }
                    
                    $_SESSION['login-ssl'] = $ssl;
                }
            }
        }

        return $ftp_con;
    }

    function gsftp_establish_session() {
        // If session is not active
        if (!session_active()) {
            // And login input fields are set
            if (check_input(LOGIN_FIELDS)) {
                // Remove leading and trailing spaces
                for ($i = 0; $i < count(LOGIN_FIELDS); $i++) {
                    $_POST[LOGIN_FIELDS[$i]] = trim($_POST[LOGIN_FIELDS[$i]]);
                }
                $server = $_POST['login-server'];
                $port = $_POST['login-port'];
                $username = $_POST['login-username'];
                $password = $_POST['login-password'];
                $ssl = isset($_POST['login-ssl']);

                filter_var($server, FILTER_SANITIZE_URL);
                filter_var($port, FILTER_SANITIZE_NUMBER_INT);
                filter_var($username, FILTER_SANITIZE_STRING);

                // Then try to connect and return true on success
                return gsftp_connect($server, $port, $username, $password, $ssl);
            } else {
                error_message('Verbindungsdaten', 'Bitte geben Sie mindestens Server, Port, Benutzername und Passwort an.', TRUE);
                return FALSE;
            }
        // If session is already active
        } else {
            $decryption_key = $_COOKIE['gsftp_aes_key'];

            $creds = [];
            for ($i = 0, $len = count(LOGIN_FIELDS); $i < $len; $i++) {
                // Then we decrypt login credentials
                $creds[LOGIN_FIELDS[$i]] = gsftp_decrypt($_SESSION[LOGIN_FIELDS[$i]], $decryption_key);
            }

            // And re-authenticate the user (keep-alive)
            return gsftp_connect($creds['login-server'], $creds['login-port'], $creds['login-username'], $creds['login-password'], $_SESSION['login-ssl']);
        }
    }

    function session_active() {
        return 
            isset($_SESSION['active']) && 
            $_SESSION['active'] === $_SESSION['auth'] &&
            isset($_COOKIE['gsftp_aes_key']) && 
            !empty($_COOKIE['gsftp_aes_key']);
    }

    function gsftp_session_bouncer($is_active, $location) {
        // If session should be active (or not), then redirect to given location
        if (session_active() === $is_active) {
            header('Location: ./' . $location);
            exit;
        }
    }

    function gsftp_encrypt($plaintext, $key) {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(AES_256_CBC));
        $cipher = openssl_encrypt($plaintext, AES_256_CBC, $key, 0, $iv);
        $cipher = $cipher . ':' . base64_encode($iv);
        return $cipher;
    }

    function gsftp_decrypt($cipher, $key) {
        $parts = explode(':', $cipher);
        $plaintext = openssl_decrypt($parts[0], AES_256_CBC, $key, 0, base64_decode($parts[1]));
        return $plaintext;
    }

    function gsftp_logout_listener() {
        global $ftp_con;
        // Logout, if the session is active and logout was authenticated due to CSRF
        if (session_active() && isset($_GET['logout']) && auth_check()) {
            // Close the FTP connection
            ftp_close($ftp_con);
            // Destroy current session
            session_unset();
            session_destroy();
            // Invalidate Cookie by setting expiration date to a date in the past
            setcookie('gsftp_aes_key', NULL, time() - AES_KEY_INVALIDATE, NULL, NULL, TRUE, TRUE);
            unset($_COOKIE['gsftp_aes_key']);
            // Redirect to login page
            header('Location: ./');
            exit;
        }
    }

    function gsftp_chdir($target) {
        global $ftp_con;
        $pwd_tmp = ftp_pwd($ftp_con);
        if (!ftp_chdir($ftp_con, $target)) {
            // If directory change fails, go back to where we came from.
            ftp_chdir($ftp_con, $pwd_tmp);
            echo error_message('Verzeichniswechsel', 'Der Ordner <b>"' . $target . '"</b> existiert nicht.', FALSE);
        }
    }

    function gsftp_chdir_listener() {
        global $ftp_con;
        // Directory change has to be authenticated due to CSRF
        if (isset($_POST['pwd']) && !empty($_POST['pwd']) && auth_check()) {
            gsftp_chdir($_POST['pwd']);
        }
    }

    function folder_exists($folder) {
        $path = realpath($folder);
        return $path && is_dir($path);
    }

    function gsftp_download_listener() {
        global $ftp_con;
        // Download has also to be authenticated due to CSRF
        if (isset($_GET['download']) && !empty($_GET['download']) && auth_check()) {
            $server_file = $_GET['download'];
            filter_var(urldecode($server_file), FILTER_SANITIZE_URL);

            // Create temporary file which is deleted if resource is closed
            $tmp_file_handle = tmpfile();
            $filename = basename($server_file);

            // Copy contents of requested file into temporary file
            if (!$tmp_file_handle || !ftp_fget($ftp_con, $tmp_file_handle, $server_file, FTP_BINARY, 0)) {
                error_message('Download', 'Die angeforderte Datei <b>"' . $filename . '"</b> konnte nicht heruntergeladen werden.');
                fclose($tmp_file_handle);
            } else {
                // Get the filepath of the temporary file containing the requested data
                $tmp_filepath = stream_get_meta_data($tmp_file_handle)['uri'];
                // Offer requested file for download
                gsftp_download_redirect($tmp_file_handle, $tmp_filepath, $filename); 
            }
        }
    }

    function gsftp_download_redirect($handle, $filepath, $filename) {
        if (file_exists($filepath)) {
            // Set HTTP headers for download
            header('Content-Description: File Transfer');
            header('Content-Type: ' . mime_content_type($filepath));
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));

            // Clean output buffer to avoid file corruption
            ob_clean();
            ob_flush();
            
            // Read data from requested file
            readfile($filepath);

            // Close temporary file resource, which deletes the copy
            fclose($handle);
            exit;
        } else {
            error_message('Download', 'Die angeforderte Datei <b>"' . $filename . '"</b> existiert nicht.');
        }
    }

    function gsftp_upload_listener() {
        global $ftp_con;
        if (isset($_POST['gsftp-send-upload']) && auth_check()) {
            // If there are files to upload
            if (!empty($_FILES)) {
                // Count how many
                $file_count = count($_FILES['gsftp-file-upload']['tmp_name']);
                $ftp_put_errors = $upload_errors = [];

                // And loop through all of them
                for ($i = 0; $i < $file_count; $i++) {
                    $file_upload_error = $_FILES['gsftp-file-upload']['error'][$i];
                    $filename = basename($_FILES['gsftp-file-upload']['name'][$i]);

                    // Checking if the current file was uploaded without any errors
                    if ($file_upload_error === UPLOAD_ERR_OK) {
                        $local_file = $_FILES['gsftp-file-upload']['tmp_name'][$i];
                        // Upload file to server and save filename of affected file on FTP error
                        if (!ftp_put($ftp_con, $filename, $local_file, FTP_BINARY)) {
                            array_push($ftp_put_errors, $filename);
                        }
                    } else {
                        // If upload error occurred, associate error message with affected file
                        $upload_errors[$filename] = PHP_FILE_UPLOAD_ERRORS[$file_upload_error];
                    }
                }

                // If there were upload errors
                if (!empty($upload_errors)) {
                    $err_str = '';
                    // Loop through all of them
                    while ($error_message = current($upload_errors)) {
                        $file_str = key($upload_errors) !== '' ? 'Datei <b>"' . $file_str . '"</b>: ' : '';
                        $err_str .= $file_str . $error_message . '<br />';
                        next($upload_errors);
                    }
                    // And print them out in one error message
                    error_message('Upload', rtrim($err_str, '<br />'));
                }
                
                // If there were FTP errors
                if (!empty($ftp_put_errors)) {
                    // Print out which files were affected
                    error_message('Upload', 'Die Datei(en) <b>"' . implode(", ", $ftp_put_errors) . '"</b> konnte(n) mit ftp_put() nicht in den FTP-Ordner <b>"' . $pwd . '"</b> geladen werden.');
                }
            }
        }
    }

    function gsftp_new_folder_listener() {
        global $ftp_con;
        if (isset($_POST['gsftp-send-new-folder']) && auth_check()) {
            if (check_input(['gsftp-new-folder-name'])) {
                $new_folder = trim($_POST['gsftp-new-folder-name']);
                filter_var($new_folder, FILTER_SANITIZE_STRING);
                if (!ftp_mkdir($ftp_con, $new_folder)) {
                    error_message('Neuer Ordner', 'Der Ordner <b>"' . $new_folder . '"</b> konnte nicht erstellt werden.');
                }
            }
        }
    }

    function gsftp_new_file_listener() {
       global $ftp_con;
        if (isset($_POST['gsftp-send-new-file']) && auth_check()) {
            if (check_input(['gsftp-new-file-name'])) {
                $new_file = trim($_POST['gsftp-new-file-name']);
                filter_var($new_file, FILTER_SANITIZE_STRING);
                $handle = tmpfile();
                // Chrome Mobile browser can't download empty files
                fwrite($handle, PHP_EOL);
				fseek($handle, 0);
                $fput = ftp_fput($ftp_con, $new_file, $handle, FTP_BINARY);
                if (!$handle || !$fput) {
                    error_message('Neue Datei', 'Die Datei <b>"' . $new_file . '"</b> konnte nicht erstellt werden.');
                }
                fclose($handle);
            }
        } 
    }

    function gsftp_rename_listener() {
        global $ftp_con;
        if (isset($_POST['gsftp-send-rename']) && auth_check()) {
            if (check_input(['gsftp-rename-name', 'gsftp-rename-oldname'])) {
                $old_name = $_POST['gsftp-rename-oldname'];
                $new_name = trim($_POST['gsftp-rename-name']);
                filter_var($old_name, FILTER_SANITIZE_STRING);
                filter_var($new_name, FILTER_SANITIZE_STRING);
                if (!ftp_rename($ftp_con, $old_name, $new_name)) {
                    error_message('Umbenennen', 'Die Datei <b>"' . $old_name . '"</b> konnte nicht in <b>"' . $new_name . '"</b> umbenannt werden.');
                }
            }
        } 
    }

    function gsftp_deletion_listener() {
        global $ftp_con;
        if (isset($_POST['gsftp-send-delete']) && auth_check()) {
            if (check_input(['gsftp-delete'])) {
                $delete_path = $_POST['gsftp-delete'];
                filter_var($delete_path, FILTER_SANITIZE_STRING);

                // In case of a directory, recursively delete all its content
                if (gsftp_is_dir($delete_path)) {
                    $pwd = ftp_pwd($ftp_con);
                    gsftp_rrmdir($delete_path);
                    // Restore the current working directory after traversing down the directory tree
                    gsftp_chdir($pwd);
                } else {
                    // In case of a file, we can simply delete it                
                    if (!ftp_delete($ftp_con, $delete_path)) {
                        error_message('Löschen', 'Die Datei <b>"' . $delete_path . '"</b> konnte nicht vom FTP-Server gelöscht werden.');
                    }
                }
            }
        }
    }

    function gsftp_rrmdir($path) {
        global $ftp_con;
        // If current path points to a directory which is not empty, we cant remove it with ftp_delete()
        if (!ftp_delete($ftp_con, $path)) {
            // Change into that directory
            gsftp_chdir($path);
            // List all of its contents
            if ($dir_list = ftp_nlist($ftp_con, '')) {
                // For each entry, except "." and ".." recursively call this function
                foreach ($dir_list as $entry) {
                    if ($entry != '.' && $entry != '..') {
                        // First we remove every files in a directory and then the directory itself
                        gsftp_rrmdir($path . DIRECTORY_SEPARATOR . $entry);
                    }
                }
            }
            // If there was an error removing a directory, return and display error message
            if (!ftp_rmdir($ftp_con, $path)) {
                error_message('Löschen', 'Der Ordner <b>"' . $path . '"</b> konnte nicht vom FTP-Server gelöscht werden.');
                return;
            }
        }
    }

    function auth_check() {
        if (isset($_GET['auth'])) {
            return $_GET['auth'] === $_SESSION['auth'];
        }

        if (isset($_POST['auth'])) {
            return $_POST['auth'] === $_SESSION['auth'];
        }

        return FALSE;
    }

    function gsftp_initial_chdir_listener() {
        global $ftp_con;
        if (check_input(['login-root'])) {
            $initial_dir = $_POST['login-root'];
            filter_var($initial_dir, FILTER_SANITIZE_URL);
            gsftp_chdir($initial_dir);
        }
    }

    function gsftp_is_dir($dir) {
        global $ftp_con;
        // An entry is a directory, if ftp_size() fails
        return ftp_size($ftp_con, $dir) == -1;
    }

    function gsftp_nav_bar() {
        global $ftp_con;
        echo '
            <nav id="gsftp-nav-bar" class="teal">
                <div class="nav-wrapper">
                    <div class="col s12 word-break">';              
                        $pwd_parts = explode(DIRECTORY_SEPARATOR, ftp_pwd($ftp_con));
                        $pwd_parts[0] = DIRECTORY_SEPARATOR;
                        $pwd_parts = array_filter($pwd_parts);

                        $subdir = '';
                        for ($i = 0, $len = count($pwd_parts); $i < $len; $i++) {
                            $subdir .= $pwd_parts[$i];
                            echo '
                                <a href="#" class="breadcrumb gsftp-nav-entry" data-pwd="' . $subdir . '" data-auth="' . $_SESSION['auth'] . '">' . $pwd_parts[$i] . '</a>
                            ';
                        }
        echo '
                    </div>
                </div>
            </nav>
        ';
    }

    function filesize_format($size) {
        $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $pow = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1000, $pow), 2, ',', ' ') . ' ' . $units[$pow];  
    }

    function gsftp_list_dir() {
        global $ftp_con;

        // Get current working directory and its contents
        $pwd = ftp_pwd($ftp_con);
        $contents = ftp_nlist($ftp_con, '');
        echo '<ul class="collapsible" id="gsftp-dir-listing" data-collapsible="accordion">';

        // Separate folders from files
        $folders = $files = [];
        foreach ($contents as $entry) {
            if (substr($pwd, -1) != DIRECTORY_SEPARATOR) {
                $pwd .= DIRECTORY_SEPARATOR;
            }

            $abs_path = $pwd . basename($entry);

            if (gsftp_is_dir($abs_path)) {
                array_push($folders, $abs_path);
            } else {
                array_push($files, $abs_path);
            }
        }

        // Sort folders and files alphabetically
        sort($folders);
        sort($files);

        // Merge folders with entries in correct order
        $entries = array_merge($folders, $files);
        foreach ($entries as $entry) {
            $entry_name = basename($entry);
            // Directory listing
            if (gsftp_is_dir($entry)) {
                echo '
                    <li '; if ($entry_name == '..') echo ' id="gsftp-up-dir"'; echo ' class="gsftp-dir-entry word-break" data-pwd="' . $entry . '" data-auth="' . $_SESSION['auth'] . '">
                        <div class="collapsible-header">
                            <i class="material-icons">folder</i>
                            <span class="gsftp-dir-entry-name">
                                ' . $entry_name . '
                            </span>
                ';
                // Do not display "edit" and "rename" for "." and ".."
                if ($entry_name != '..' && $entry_name != '.') {
                        echo '  
                            <span class="badge">
                                <span class="badge-icons">
                                    <a data-entryname="' . $entry_name . '" data-folder="true" class="gsftp-open-rename">
                                        <i class="material-icons no-margin amber-text text-darken-4">edit</i>
                                    </a>
                                    <a data-path="' . $entry . '" data-entryname="' . $entry_name . '" data-folder="true" class="gsftp-open-delete">
                                        <i class="material-icons no-margin red-text text-darken-4">delete</i>
                                    </a>
                                </span>
                            </span>
                        ';
                }
                echo'
                        </div>
                    </li>
                ';
            } else {
                // File listing
                $filesize = filesize_format(ftp_size($ftp_con, $entry));
                echo '
                    <li class="word-break">
                        <div class="collapsible-header">
                            <i class="material-icons">insert_drive_file</i>
                            <span class="gsftp-file-entry-name">
                                ' . $entry_name . '
                            </span>
                            <span class="badge">
                                ' . $filesize . '
                            </span>
                        </div>
                        <div class="collapsible-body white center-align">
                            <a data-entryname="' . $entry_name . '" href="?download=' . urlencode($entry) . '&auth=' . $_SESSION['auth'] . '" class="waves-effect waves-light btn gsftp-file-download-link">
                                <i class="material-icons">cloud_download</i>
                            </a>
                            <a data-entryname="' . $entry_name . '" data-folder="false" class="gsftp-open-rename waves-effect waves-light amber darken-4 btn">
                                <i class="material-icons">edit</i>
                            </a>
                            <a data-path="' . $entry . '" data-entryname="' . $entry_name . '" data-folder="false" class="gsftp-open-delete waves-effect waves-light red darken-4 btn">
                                <i class="material-icons">delete</i>
                            </a>
                        </div>
                    </li>
                ';
            }
        }

        echo '</ul>';
    }
?>