<?php
    session_start();
    require_once('functions.php');

    // If login not send and session invalid, bounce
    if (!isset($_POST['login-send'])) {
        gsftp_session_bouncer(FALSE, 'index.php');
    }

    // Start the output buffer, listeners might echo
    ob_start();
    echo head('Main');

    // Global ftp connection handle for session
    $ftp_con = gsftp_establish_session();

    // If true, session is valid
    if ($ftp_con !== FALSE):
?>
<body>
    <div id="gsftp-wrapper" class="row no-bottom-margin">
        
<?php 
    // Listen for any user action
    gsftp_listen();

    // Flush output buffer
    ob_flush();
?>
        <div id="gsftp-main" class="col s12 m12 l8 push-l2 xl6 push-xl3">
            <div class="row no-bottom-margin">
                <?php
                    // Output the navigation bar and the file listing
                    gsftp_nav_bar();
                    gsftp_list_dir();
                ?>
            </div>
        <!-- End of main -->
        </div>

        <div id="gsftp-footer" class="col s12 m12 l8 push-l2 xl6 push-xl3 no-padding">
            <div class="row center-align">
                <a id="gsftp-open-upload" class="waves-effect waves-light teal btn-large"><i class="material-icons">cloud_upload</i></a>
                <a id="gsftp-open-new-folder" class="waves-effect waves-light amber darken-4 btn-large"><i class="material-icons">create_new_folder</i></a>
                <a id="gsftp-open-new-file" class="waves-effect waves-light blue darken-4 btn-large"><i class="material-icons">insert_drive_file</i></a>
                <a id="gsftp-logout" class="waves-effect waves-light red darken-4 btn-large" href="?logout&auth=<?php echo $_SESSION['auth']; ?>">
                    <i class="material-icons">exit_to_app</i>
                </a>
            </div>

            <div class="row no-bottom-margin">
                <div class="col s4 left-align">
                    <a class="btn-floating btn-large waves-effect waves-light teal" id="gsftp-toggle-voice"><i class="material-icons">mic_off</i></a> 
                </div>
                <div class="col s4 center-align">
                    <div id="gsftp-logo">
                        <img src="img/logo.svg" alt="GSftp Logo" data-pwd="<?php echo DIRECTORY_SEPARATOR; ?>" data-auth="<?php echo $_SESSION['auth']; ?>" />
                    </div>
                    <div id="gsftp-ssl-status">
                        <?php
                            $color = $icon = '';
                            if ($_SESSION['login-ssl']) {
                                $color = 'teal-text'; $icon = 'lock';
                            } else {
                                $color = 'red-text text-darken-4'; $icon = 'lock_open';
                            }
                            echo '<i class="material-icons ' . $color . '">' . $icon . '</i>';
                        ?>
                    </div>
                </div>
                <div class="col s4 right-align">
                    <a class="btn-floating btn-large waves-effect waves-light teal" id="gsftp-toggle-fullscreen"><i class="material-icons">fullscreen</i></a>
                </div>
            </div>
        <!-- End of footer -->
        </div>

        <div id="gsftp-action-modals">
            <!-- Upload modal -->
            <div id="gsftp-upload" class="modal">
                <div class="modal-content">
                    <h5>Dateien hochladen</h5>
                    <form enctype="multipart/form-data" method="post">
                        <input type="hidden" name="pwd" value="<?php echo ftp_pwd($ftp_con); ?>" />
                        <input type="hidden" name="auth" value="<?php echo $_SESSION['auth']; ?>" />
                        <input type="hidden" name="gsftp-send-upload" />
                        <div class="file-field input-field">
                            <div class="btn">
                                <i class="material-icons">note_add</i>
                                <input type="file" id="gsftp-file-upload" name="gsftp-file-upload[]" multiple="multiple" />
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Dateien auswählen" required="required" />
                            </div>
                        </div>
                        <div class="center-align">
                            <button class="btn waves-effect waves-light" type="submit" id="gsftp-send-upload">Hochladen
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                    <div class="progress hidden">
                        <div class="indeterminate"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Schließen</a>
                </div>
            </div>

            <!-- New folder modal -->
            <div id="gsftp-new-folder" class="modal">
                <div class="modal-content">
                    <h5>Ordner erstellen</h5>
                    <form method="post">
                        <input type="hidden" name="pwd" value="<?php echo ftp_pwd($ftp_con); ?>" />
                        <input type="hidden" name="auth" value="<?php echo $_SESSION['auth']; ?>" />
                        <input type="hidden" name="gsftp-send-new-folder" />
                        <div class="input-field col s12">
                            <i class="material-icons prefix">folder</i>
                            <input id="gsftp-new-folder-name" name="gsftp-new-folder-name" type="text" required="required" />
                            <label for="gsftp-new-folder-name">Neuer Ordner</label>
                        </div>
                        <div class="center-align">
                            <button class="btn waves-effect waves-light amber darken-4" type="submit" id="gsftp-send-new-folder">Erstellen
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Schließen</a>
                </div>
            </div>

            <!-- New file modal -->
            <div id="gsftp-new-file" class="modal">
                <div class="modal-content">
                    <h5>Datei erstellen</h5>
                    <form method="post">
                        <input type="hidden" name="pwd" value="<?php echo ftp_pwd($ftp_con); ?>" />
                        <input type="hidden" name="auth" value="<?php echo $_SESSION['auth']; ?>" />
                        <input type="hidden" name="gsftp-send-new-file" />
                        <div class="input-field col s12">
                            <i class="material-icons prefix">insert_drive_file</i>
                            <input id="gsftp-new-file-name" name="gsftp-new-file-name" type="text" required="required" />
                            <label for="gsftp-new-file-name">Neue Datei</label>
                        </div>
                        <div class="center-align">
                            <button class="btn waves-effect waves-light blue darken-4" type="submit" id="gsftp-send-new-file">Erstellen
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Schließen</a>
                </div>
            </div>

            <!-- Rename modal -->
            <div id="gsftp-rename" class="modal">
                <div class="modal-content">
                    <h5><span class="entry-type"></span> umbenennen</h5>
                    <form method="post">
                        <input type="hidden" name="pwd" value="<?php echo ftp_pwd($ftp_con); ?>" />
                        <input type="hidden" name="auth" value="<?php echo $_SESSION['auth']; ?>" />
                        <input type="hidden" name="gsftp-rename-oldname" value="" />
                        <input type="hidden" name="gsftp-send-rename" />
                        <div class="input-field col s12">
                            <i class="material-icons prefix">edit</i>
                            <input id="gsftp-rename-name" name="gsftp-rename-name" type="text" required="required" />
                            <label for="gsftp-rename-name">Neuer Name</label>
                        </div>
                        <div class="center-align">
                            <button class="btn waves-effect waves-light amber darken-4" type="submit" id="gsftp-send-rename">Umbenennen
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Schließen</a>
                </div>
            </div>

            <!-- Delete modal -->
            <div id="gsftp-delete" class="modal">
                <div class="modal-content">
                    <h5><span class="entry-type"></span> löschen</h5>
                    Sind Sie sicher, dass Sie <span class="entry-pronoun"></span> <span class="entry-type"></span> <b>"<span class="entry-name"></span>"</b> löschen möchten?
                    <form method="post">
                        <input type="hidden" name="pwd" value="<?php echo ftp_pwd($ftp_con); ?>" />
                        <input type="hidden" name="auth" value="<?php echo $_SESSION['auth']; ?>" />
                        <input type="hidden" name="gsftp-delete" value="" />
                        <input type="hidden" name="gsftp-send-delete" />
                        <p class="center-align">
                            <button class="btn waves-effect waves-light red darken-4" type="submit" id="gsftp-send-delete">Löschen
                                <i class="material-icons right">send</i>
                            </button>
                        </p>
                    </form>
                    <div class="progress hidden">
                        <div class="indeterminate red darken-4"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Schließen</a>
                </div>
            </div>

            <!-- Connection info modal -->
            <div id="gsftp-connection-info" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h5>FTP Verbindung</h5>
                    <ul class="collection">
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">cloud</i>
                            <span class="title">FTP Server</span>
                            <p>
                                <?php
                                    $decryption_key = $_COOKIE['gsftp_aes_key'];
                                    $server_name = gsftp_decrypt($_SESSION['login-server'], $decryption_key);
                                    $server_port = gsftp_decrypt($_SESSION['login-port'], $decryption_key);
                                    echo $server_name . '<br />Port ' . $server_port;
                                ?>
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">person</i>
                            <span class="title">Benutzername</span>
                            <p>
                                <?php echo gsftp_decrypt($_SESSION['login-username'], $decryption_key); ?>
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">folder</i>
                            <span class="title">Verzeichnis</span>
                            <p>
                                <?php echo ftp_pwd($ftp_con); ?>
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">lock</i>
                            <span class="title">SFTP</span>
                            <p>
                                <?php echo $_SESSION['login-ssl'] ? 'Aktiv' : 'Nicht aktiv'; ?>
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">http</i>
                            <span class="title">HTTPS</span>
                            <p>
                                <?php 
                                    echo $_SERVER['HTTPS'] ? 'Aktiv' : 'Nicht aktiv'; 
                                    echo '<br />' . $_SERVER['SERVER_PROTOCOL'] . ', ' . $_SERVER['HTTP_CONNECTION'];
                                ?>
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">import_export</i>
                            <span class="title">Webserver</span>
                            <p>
                                <?php echo $_SERVER['SERVER_NAME'] . '<br />Port ' . $_SERVER['SERVER_PORT']; ?>
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">place</i>
                            <span class="title">Client IP</span>
                            <p>
                                <?php echo $_SERVER['REMOTE_ADDR'] . '<br />Port ' . $_SERVER['REMOTE_PORT']; ?>
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle teal">memory</i>
                            <span class="title">FTP Systemtyp</span>
                            <p>
                                <?php echo ftp_systype($ftp_con); ?>
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Schließen</a>
                </div>
            </div>
        <!-- End of action modals -->
        </div>
    <!-- End of wrapper -->
    </div>
</body>
<?php endif; ?>
</html>