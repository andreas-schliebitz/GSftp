$(function() {
    $('.modal').modal();

    $(static_div).on('click', '#gsftp-open-upload', function() {
        $('#gsftp-upload').modal('open');
    });

    $(static_div).on('click', '#gsftp-open-new-folder', function() {
        $('#gsftp-new-folder').modal('open');
    });

    $(static_div).on('click', '#gsftp-open-new-file', function() {
        $('#gsftp-new-file').modal('open');
    });

    // jQuery element delegation required, because content is changed dynamically via AJAX
    $(static_div).on('click', '.gsftp-open-rename', function(e) {
        $('#gsftp-rename').modal('open');
        var isfolder = $(this).attr('data-folder');

        var entry_type = isfolder == 'true' ? 'Ordner' : 'Datei';
        $('#gsftp-rename span.entry-type').html(entry_type);
        
        var oldname = $(this).attr('data-entryname');
        $('#gsftp-rename input[name=gsftp-rename-oldname]').val(oldname);
        $('#gsftp-rename-name').val(oldname);
        Materialize.updateTextFields();

        e.stopPropagation();
    });

    $(static_div).on('click', '.gsftp-open-delete', function(e) {
        $('#gsftp-delete').modal('open');

        var isfolder = $(this).attr('data-folder') == 'true';
        var entry_type = isfolder ? 'Ordner' : 'Datei';
        var entry_pronoun = isfolder ? 'den' : 'die';

        $('#gsftp-delete span.entry-type').html(entry_type);
        $('#gsftp-delete span.entry-pronoun').html(entry_pronoun);

        var filepath = $(this).attr('data-path');
        $('#gsftp-delete span.entry-name').html(filepath);
        $('#gsftp-delete input[name=gsftp-delete]').val(filepath);

        e.stopPropagation();
    });

    $(static_div).on('click', '.gsftp-dir-entry, .gsftp-nav-entry', function() {
        ajax_nav(this);
    });

    $(static_div).on('click', '.gsftp-error-close', function() {
        if ($(this).attr('href') == '#') {
            $(this).closest('.gsftp-error').hide(0);
        }
    });

    $(static_div).on('click', '#gsftp-logo img', function() {
        $('#gsftp-connection-info').modal('open');
    });

    $(static_div).on('click', '#gsftp-toggle-fullscreen', function() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            enter_fullscreen();
            is_fullscreen = true;
            $(this).html('<i class="material-icons">fullscreen_exit</i>');
        } else {
            exit_fullscreen();
            is_fullscreen = false;
            $(this).html('<i class="material-icons">fullscreen</i>');
        }
    });
});