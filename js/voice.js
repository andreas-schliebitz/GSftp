$(function() {
    if (annyang) {
        // Custom commands for speech recognition
        var commands = {
            'Datei hochladen': function() {
                $('#gsftp-upload').modal('open');
            },
            'hochladen': function() {
                if ($('#gsftp-file-upload').get(0).files.length !== 0) {
                    $('#gsftp-send-upload')[0].click();
                    $('#gsftp-upload').modal('close');
                }
            },
            'neuer Ordner *dirname': function(dirname) {
                $('#gsftp-new-folder').modal('open');
                $('#gsftp-new-folder-name').val(normalize(dirname));
                Materialize.updateTextFields();
            },
            'Ordner erstellen': function() {
                if ($('#gsftp-new-folder-name').val()) {
                    $('#gsftp-send-new-folder')[0].click();
                    $('#gsftp-new-folder').modal('close');
                }
            },
            'neuer Ordner': function() {
                $('#gsftp-new-folder').modal('open');
            },
            'neue Datei *filename': function(filename) {
                $('#gsftp-new-file').modal('open');
                $('#gsftp-new-file-name').val(normalize(filename));
                Materialize.updateTextFields();
            },
            'neue Datei': function() {
                $('#gsftp-new-file').modal('open');
            },
            'Datei erstellen': function() {
                if ($('#gsftp-new-file-name').val()) {
                    $('#gsftp-send-new-file')[0].click();
                    $('#gsftp-new-file').modal('close');
                }
            },
            'schließen': function() {
                $('.modal').modal('close');
                $('.modal input').val('');
                var close_error = $('.gsftp-error-close')[0];
                trigger_click(close_error);
            },
            'abmelden': function() {
                $('#gsftp-logout')[0].click();
            },
            'hoch': function() {
                var up = $('#gsftp-up-dir')[0];
                trigger_click(up);
            },
            'betrete *dir': function(dir) {
                var href = $('.gsftp-dir-entry-name').filter(function() {
                    var string = $(this).text();
                    return check_match(string, dir);
                }).closest('li')[0];
                trigger_click(href);
            },
            '*filename herunterladen': function(filename) {
                var href = find_target('.gsftp-file-download-link', 'data-entryname', filename);
                trigger_click(href);
            },
            '*entry umbenennen in *newentry': function(entry, newentry) {
                var button = find_target('.gsftp-open-rename', 'data-entryname', entry);
                if (trigger_click(button)) {
                    $('#gsftp-rename-name').val(normalize(newentry));
                }
            },
            '*entry umbenennen': function(entry, newentry) {
                var button = find_target('.gsftp-open-rename', 'data-entryname', entry);
                trigger_click(button);
            },
            'umbenennen': function() {
                if ($('#gsftp-rename-name').val()) {
                    $('#gsftp-send-rename')[0].click();
                    $('#gsftp-rename').modal('close');
                }
            },
            'lösche *entry': function(entry) {
                var button = find_target('.gsftp-open-delete', 'data-entryname', entry);
                trigger_click(button);
            },
            'löschen': function() {
                $('#gsftp-send-delete')[0].click();
            },
            'info': function() {
                $('#gsftp-logo img')[0].click();
            },
            'stop': function() {
                annyang.abort();
                $('#gsftp-toggle-voice').html('<i class="material-icons">mic_off</i>');
            },
        };

        // Set recognizer language and add commands
        annyang.setLanguage('de-DE');
        annyang.addCommands(commands);
    }

    $(static_div).on('click', '#gsftp-toggle-voice', function() {
        if (annyang) {
            if (annyang.isListening()) {
                annyang.abort();
                is_listening = false;
                $(this).html('<i class="material-icons">mic_off</i>');
            } else {
                annyang.start();
                is_listening = true;
                $(this).html('<i class="material-icons">mic</i>');
            }
        } else {
            alert('Spracherkennung ist leider nicht verfügbar.')
        }    
    });
});