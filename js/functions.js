var static_div = '#gsftp-wrapper';
var is_fullscreen = is_listening = false;

function ajax_nav(element) {
    // Get current directory and auth token
    var p = $(element).attr('data-pwd');
    var a =  $(element).attr('data-auth');

    $.ajax({
        type: 'post',
        url: 'gsftp.php',
        data: {pwd: p, auth: a},
        success: function(response) {
            // Refresh page content
            update_div(static_div, response);
            materialize_init();
        },
        error: function() {
            alert('Ihre Anfrage konnte nicht versendet werden.');
        }
    });
}

function materialize_init() {
    $('.collapsible').collapsible();
    $('.modal').modal();
    if (is_fullscreen) $('#gsftp-toggle-fullscreen').html('<i class="material-icons">fullscreen_exit</i>');
    if (is_listening) $('#gsftp-toggle-voice').html('<i class="material-icons">mic</i>');
}

function update_div(target, response) {
    // Filter out the inner html of requested element from response
    var html = $(response).filter(target).html();
    // Display the inner HTML from above as inner HTML of target
    $(target).html(html);
}

function check_match(string, recognized) {
    // Remove leading and trailing spaces, convert to lower case
    string = $.trim(string.toLowerCase());
    recognized = $.trim(recognized.toLowerCase());

    // Remove whitespaces if there are any
    if (string.indexOf(' ') !== -1) {
        string = string.split(' ').join('');
    }

    if (recognized.indexOf(' ') !== -1) {
        recognized = recognized.split(' ').join('');
    }

    return string === recognized;
}

function normalize(recognized) {
    // File and folder names should be lower case and without spaces
    recognized = recognized.toLowerCase();
    if (recognized.indexOf(' ') !== -1) {
        recognized = recognized.split(' ').join('');
    }
    return recognized;
}

function find_target(class_name, attr_name, recognized) {
    // Trying to bind recognized string (file or folder name) to clickable HTML element
    return $(class_name).filter(function() {
                var string = $(this).attr(attr_name);
                return check_match(string, recognized);
            })[0];
}

function trigger_click(target) {
    // If a clickable HTML element was successfully matched
    if (typeof target !== 'undefined') {
        // Click that element and return true
        target.click();
        return true;
    }
    return false;
}

function enter_fullscreen() {
    if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen();
    }
}

function exit_fullscreen() {
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
}