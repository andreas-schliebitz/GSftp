$(function() {
    // Listening for form submit action
    $(static_div).on('submit', 'form', function(e) {
        // Prevent the Browser from submitting the form
        e.preventDefault();

        // Get the form
        var form = $(this);

        // Get the progress bar, if existing
        var progress = form.parent().find('.progress');

        // Submit the form via AJAX
        $.ajax({
            url: 'gsftp.php', 
            type: 'post',
            data: new FormData(form[0]),
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                progress.show();
            },
            complete: function(){
                progress.hide();
            },
            success: function(response) {
                $('.modal-overlay').remove();
                update_div(static_div, response);
                materialize_init();
            },
            error: function() {
                alert('Ihre Anfrage konnte nicht versendet werden.');
            }
        });
    });
});